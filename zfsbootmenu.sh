#!/usr/bin/env bash

dmesg | grep -i efivars

sudo -i

source /etc/os-release
export ID

cat <<EOF > /etc/apt/sources.list
deb http://deb.debian.org/debian testing main contrib
deb-src http://deb.debian.org/debian testing main contrib
EOF
apt update

apt install debootstrap gdisk dkms linux-headers-"$(uname -r)"
apt install zfsutils-linux

/sbin/modprobe zfs
zfs --version

zgenhostid -f 0x00bab10c

export BOOT_DISK="/dev/sda"
export BOOT_PART="1"
export BOOT_DEVICE="${BOOT_DISK}${BOOT_PART}"

export POOL_DISK="/dev/sda"
export POOL_PART="2"
export POOL_DEVICE="${POOL_DISK}${POOL_PART}"

zpool labelclear -f "$POOL_DISK"

wipefs -a "$POOL_DISK"
wipefs -a "$BOOT_DISK"

sgdisk --zap-all "$POOL_DISK"
sgdisk --zap-all "$BOOT_DISK"

sgdisk -n "${BOOT_PART}:1m:+512m" -t "${BOOT_PART}:ef00" "$BOOT_DISK"

sgdisk -n "${POOL_PART}:0:-10m" -t "${POOL_PART}:bf00" "$POOL_DISK"

zpool create -f -o ashift=12 \
 -O compression=lz4 \
 -O acltype=posixacl \
 -O xattr=sa \
 -O relatime=on \
 -o autotrim=on \
 -m none zfs "$POOL_DEVICE"

zfs create -o mountpoint=none zfs/ROOT
zfs create -o mountpoint=/ -o canmount=noauto zfs/ROOT/${ID}
zfs create -o mountpoint=/home zfs/home

zpool set bootfs=zfs/ROOT/${ID} zfs

zpool export zfs
zpool import -N -R /mnt zfs
zfs mount zfs/ROOT/${ID}
zfs mount zfs/home

mount | grep mnt

udevadm trigger

debootstrap testing /mnt

cp /etc/hostid /mnt/etc
cp /etc/resolv.conf /mnt/etc

mount -t proc proc /mnt/proc
mount -t sysfs sys /mnt/sys
mount -B /dev /mnt/dev
mount -t devpts pts /mnt/dev/pts
chroot /mnt /bin/bash

####################################

echo 'HOSTYON' > /etc/hostname
echo -e '127.0.1.1\tHOSTYON' >> /etc/hosts

# Set password
passwd

cat <<EOF > /etc/apt/sources.list
deb http://deb.debian.org/debian testing main contrib
deb-src http://deb.debian.org/debian testing main contrib

deb http://deb.debian.org/debian-security testing-security main contrib
deb-src http://deb.debian.org/debian-security/ testing-security main contrib

deb http://deb.debian.org/debian testing-updates main contrib
deb-src http://deb.debian.org/debian testing-updates main contrib

deb http://deb.debian.org/debian testing-backports main contrib
deb-src http://deb.debian.org/debian testing-backports main contrib
EOF

apt update

echo "Europe/Berlin" > /etc/timezone && \
    dpkg-reconfigure -f noninteractive tzdata && \
    sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && \
    sed -i -e 's/# de_DE.UTF-8 UTF-8/de_DE.UTF-8 UTF-8/' /etc/locale.gen && \
    echo 'LANG="de_DE.UTF-8"'>/etc/default/locale && \
    dpkg-reconfigure --frontend=noninteractive locales && \
    update-locale LANG=de_DE.UTF-8

apt install locales keyboard-configuration console-setup
#dpkg-reconfigure locales tzdata keyboard-configuration console-setup

apt install linux-headers-amd64 linux-image-amd64 zfs-initramfs dosfstools
echo "REMAKE_INITRD=yes" > /etc/dkms/zfs.conf

systemctl enable zfs.target
systemctl enable zfs-import-cache
systemctl enable zfs-mount
systemctl enable zfs-import.target

update-initramfs -c -k all

# Install ZFSBootMenu
zfs set org.zfsbootmenu:commandline="quiet" zfs/ROOT
mkfs.vfat -F32 "$BOOT_DEVICE"

cat << EOF >> /etc/fstab
$( blkid | grep "$BOOT_DEVICE" | cut -d ' ' -f 2 ) /boot/efi vfat defaults 0 0
EOF
mkdir -p /boot/efi
mount /boot/efi

apt install curl
mkdir -p /boot/efi/EFI/ZBM
curl -o /boot/efi/EFI/ZBM/VMLINUZ.EFI -L https://get.zfsbootmenu.org/efi
cp /boot/efi/EFI/ZBM/VMLINUZ.EFI /boot/efi/EFI/ZBM/VMLINUZ-BACKUP.EFI

mount -t efivarfs efivarfs /sys/firmware/efi/efivars
apt install efibootmgr
efibootmgr -c -d "$BOOT_DISK" -p "$BOOT_PART" \
  -L "ZFSBootMenu (Backup)" \
  -l '\EFI\ZBM\VMLINUZ-BACKUP.EFI'
efibootmgr -c -d "$BOOT_DISK" -p "$BOOT_PART" \
  -L "ZFSBootMenu" \
  -l '\EFI\ZBM\VMLINUZ.EFI'

#exit chroot
exit
umount -n -R /mnt

zpool export zfs
reboot


